package com.booking.fooddeliveryapp.SharedPreferences;

import android.content.Context;

public class DataLocalManager {
    private static final String JWT_TOKEN = "JWT_TOKEN";
    private static DataLocalManager instance;
    private MySharedPreferences mySharedPreferences;

    public static void init(Context context) {
        instance = new DataLocalManager();
        instance.mySharedPreferences = new MySharedPreferences(context);
    }

    public static DataLocalManager getInstance() {
        if (instance == null)
            instance = new DataLocalManager();
        return instance;
    }

    public static void setTokenLogin(String token) {
        DataLocalManager.getInstance().mySharedPreferences.putStringValue(JWT_TOKEN, token);
    }

    public static String getTokenLogin() {
        return DataLocalManager.getInstance().mySharedPreferences.getStringValue(JWT_TOKEN);
    }

    public static void removeTokenLogin() {
        DataLocalManager.getInstance().mySharedPreferences.removeStringValue(JWT_TOKEN);
    }
}
