package com.booking.fooddeliveryapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.booking.fooddeliveryapp.SharedPreferences.DataLocalManager;
import com.booking.librs.InterFaces.Methods;
import com.booking.librs.Models.AccessToken;
import com.booking.librs.Models.UserLogin;
import com.booking.librs.RetrofitClient;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    Button btnLogin;
    EditText edtEmail, edtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        String token = DataLocalManager.getTokenLogin();
        init();
        if (!Objects.equals(token, ""))
        {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Methods methods = RetrofitClient.getRetrofitInstance().create(Methods.class);
                UserLogin userLogin = new UserLogin(
                        edtEmail.getText().toString(),
                        edtPassword.getText().toString()
                );

                Call<AccessToken> call = methods.Login(userLogin);
                call.enqueue(new Callback<AccessToken>() {
                    @Override
                    public void onResponse(Call<AccessToken> call, Response<AccessToken> response) {
                        AccessToken accessToken = response.body();
                        if (accessToken == null) {
                            Toast.makeText(LoginActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        DataLocalManager.setTokenLogin(accessToken.token);
                        Toast.makeText(LoginActivity.this, "LoginSuccess", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        finish();
                    }

                    @Override
                    public void onFailure(Call<AccessToken> call, Throwable t) {
                        Toast.makeText(LoginActivity.this, "LoginFailed", Toast.LENGTH_SHORT).show();
                        Toast.makeText(LoginActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });
    }

    public void init() {
        btnLogin = findViewById(R.id.btn_Login);
        edtEmail = findViewById(R.id.edt_Email);
        edtPassword = findViewById(R.id.edt_Password);
    }
}