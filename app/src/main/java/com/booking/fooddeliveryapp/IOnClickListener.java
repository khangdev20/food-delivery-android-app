package com.booking.fooddeliveryapp;

import com.booking.librs.Models.Category;

public interface IOnClickListener {
    void onClickDeleteItem(Category category);
    void onClickGoToDetail(Category category);
}
