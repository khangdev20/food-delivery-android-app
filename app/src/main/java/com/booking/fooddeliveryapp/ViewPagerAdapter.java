package com.booking.fooddeliveryapp;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.booking.fooddeliveryapp.Fragment.HomeFragment;
import com.booking.fooddeliveryapp.Fragment.ProfileFragment;
import com.booking.fooddeliveryapp.Fragment.StoreFragment;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    public ViewPagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 1:
                return new StoreFragment();
            case 2:
                return new ProfileFragment();
            case 0:
            default:
                return new HomeFragment();
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
}
