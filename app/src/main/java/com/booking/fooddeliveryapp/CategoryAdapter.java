package com.booking.fooddeliveryapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.booking.fooddeliveryapp.SharedPreferences.DataLocalManager;
import com.booking.librs.InterFaces.Methods;
import com.booking.librs.Models.BaseResponse;
import com.booking.librs.Models.Category;
import com.booking.librs.Models.TestModel;
import com.booking.librs.RetrofitClient;

import java.util.List;
import java.util.zip.DataFormatException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {

    private Context mContext;
    private List<Category> categories;
    private IOnClickListener iOnClickListener;

    public CategoryAdapter(Context mContext, IOnClickListener listener) {
        this.mContext = mContext;
        this.iOnClickListener = listener;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<Category> list) {
        this.categories = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent, false);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        Category category = categories.get(position);
        if (category == null) {
            return;
        }
        holder.tvCategoryName.setText(category.getName());
        holder.layoutItemCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iOnClickListener.onClickGoToDetail(category);
            }
        });
        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iOnClickListener.onClickDeleteItem(category);

            }
        });
    }

    @Override
    public int getItemCount() {
        if (categories != null) {
            return categories.size();
        }
        return 0;
    }

    public static class CategoryViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvCategoryName;
        private final CardView layoutItemCategory;
        private Button btnDelete;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCategoryName = itemView.findViewById(R.id.tv_name_category);
            layoutItemCategory = itemView.findViewById(R.id.layout_item_category);
            btnDelete = itemView.findViewById(R.id.btn_delete);
        }
    }
}
