package com.booking.fooddeliveryapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.booking.fooddeliveryapp.SharedPreferences.DataLocalManager;
import com.booking.librs.InterFaces.Methods;
import com.booking.librs.Models.BaseResponse;
import com.booking.librs.Models.RestaurantCreate;
import com.booking.librs.RetrofitClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateRestaurantActivity extends AppCompatActivity {
    private Button btnSubmit;
    private EditText edtName, edtAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_restaurant);
        init();
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Methods methods = RetrofitClient.getRetrofitInstance().create(Methods.class);
                String name = edtName.getText().toString();
                String address = edtAddress.getText().toString();
                String token = DataLocalManager.getTokenLogin();
                if (name.equals("") && address.equals("")) {
                    Toast.makeText(CreateRestaurantActivity.this, "Name or Address is empty!", Toast.LENGTH_LONG).show();
                    return;
                }
                RestaurantCreate restaurantCreate = new RestaurantCreate(
                        name,
                        address
                );
                Call<BaseResponse> call = methods.CreateNewRes("Bearer " + token, restaurantCreate);
                call.enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        BaseResponse baseResponse = response.body();
                        if (baseResponse == null) return;
                        if (baseResponse.statusCode == 200) {
                            onBackPressed();
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        Toast.makeText(CreateRestaurantActivity.this, "Failed" + t, Toast.LENGTH_LONG).show();
                    }
                });

            }
        });


    }

    public void init() {
        btnSubmit = findViewById(R.id.btn_Submit);
        edtName = findViewById(R.id.edt_Name);
        edtAddress = findViewById(R.id.edt_Address);
    }
}