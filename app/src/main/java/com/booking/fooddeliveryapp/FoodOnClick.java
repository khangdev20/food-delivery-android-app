package com.booking.fooddeliveryapp;

import com.booking.librs.Models.Food;

public interface FoodOnClick {
    void onLongClickFood(Food food);
    void onClickFood(Food food);
}
