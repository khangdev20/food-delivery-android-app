package com.booking.fooddeliveryapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.booking.fooddeliveryapp.SharedPreferences.DataLocalManager;
import com.booking.librs.InterFaces.Methods;
import com.booking.librs.Models.BaseResponse;
import com.booking.librs.Models.Category;
import com.booking.librs.Models.Food;
import com.booking.librs.RetrofitClient;

import java.util.List;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailCategory extends AppCompatActivity {

    private TextView tvName;
    private RecyclerView rcvFood;
    private Button btnInsertFood;
    Methods methods = RetrofitClient.getRetrofitInstance().create(Methods.class);
    String token = DataLocalManager.getTokenLogin();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_category);

    }

    private void goToInsertFood(String cateId) {
        Intent i = new Intent(DetailCategory.this, CreateFoodActivity.class);
        i.putExtra("cateId", cateId);
        i.putExtra("cateName", tvName.getText());
        startActivity(i);
    }

    private void setRcvFood(List<Food> foods) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(DetailCategory.this, LinearLayoutManager.VERTICAL, false);
        rcvFood.setLayoutManager(linearLayoutManager);
        FoodAdapter foodAdapter = new FoodAdapter(DetailCategory.this, new FoodOnClick() {
            @Override
            public void onLongClickFood(Food food) {
                android.app.AlertDialog.Builder builder = new AlertDialog.Builder(DetailCategory.this);
                builder.setCancelable(true);
                builder.setTitle("Do you want to delete " + food.name + "?");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        onDeleteFood(food);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                builder.show();
            }

            @Override
            public void onClickFood(Food food) {

            }
        });
        foodAdapter.setData(foods);
        rcvFood.setAdapter(foodAdapter);
    }

    private void onDeleteFood(Food food) {

        Call<BaseResponse> call = methods.DeleteFood("Bearer " + token, food.id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                onPostResume();
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {

            }
        });


    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        tvName = findViewById(R.id.tv_name_category);
        rcvFood = findViewById(R.id.rcv_food);
        btnInsertFood = findViewById(R.id.btn_insert_food);
        Bundle bundle = getIntent().getExtras();
        String cateId = bundle.getString("cateId");
        Call<Category> call = methods.GetCategory("Bearer " + token, UUID.fromString(cateId));
        call.enqueue(new Callback<Category>() {
            @Override
            public void onResponse(Call<Category> call, Response<Category> response) {
                Category category = response.body();
                if (category != null) {
                    tvName.setText(category.getName());
                    setRcvFood(category.getFoods());
                }
            }

            @Override
            public void onFailure(Call<Category> call, Throwable t) {

            }
        });

        btnInsertFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToInsertFood(cateId);
            }
        });
    }
}