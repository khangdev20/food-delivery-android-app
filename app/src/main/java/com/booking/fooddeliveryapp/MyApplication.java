package com.booking.fooddeliveryapp;

import android.app.Application;

import com.booking.fooddeliveryapp.SharedPreferences.DataLocalManager;


public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        DataLocalManager.init(getApplicationContext());
    }
}
