package com.booking.fooddeliveryapp.Fragment;

import static android.widget.Toast.*;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.booking.fooddeliveryapp.CreateRestaurantActivity;
import com.booking.fooddeliveryapp.LoginActivity;
import com.booking.fooddeliveryapp.MainActivity;
import com.booking.fooddeliveryapp.R;
import com.booking.fooddeliveryapp.SharedPreferences.DataLocalManager;
import com.booking.librs.InterFaces.Methods;
import com.booking.librs.Models.User;
import com.booking.librs.RetrofitClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragment extends Fragment {
    private TextView tvName, tvEmail, tvPhone, tvRoles;
    private Button btnLogout;
    private View mView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.profile_fragment, container, false);
        init();
        Methods methods = RetrofitClient.getRetrofitInstance().create(Methods.class);
        String token = DataLocalManager.getTokenLogin();
        Call<User> call = methods.GetUserProfile("Bearer " + token);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User user = response.body();
                System.out.println(user);
                if (user == null) {
                    makeText(mView.getContext(), "User is null", LENGTH_SHORT).show();
                    return;
                }
                tvName.setText(user.name);
                tvEmail.setText(user.email);
                tvPhone.setText(user.phone);
                if (user.restaurant != null) {
                    tvRoles.setText(user.restaurant.name);
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                makeText(mView.getContext(), "feo" + t, LENGTH_SHORT).show();
                startActivity(new Intent(mView.getContext(), LoginActivity.class));
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mView.getContext());
                builder.setCancelable(true);
                builder.setTitle("Do you want to sign out?");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        signOut();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
                builder.show();
            }
        });

        return mView;
    }

    private void init() {
        tvName = mView.findViewById(R.id.tv_name);
        tvEmail = mView.findViewById(R.id.tv_email);
        tvPhone = mView.findViewById(R.id.tv_phone);
        tvRoles = mView.findViewById(R.id.tv_roles);
        btnLogout = mView.findViewById(R.id.btn_logout);
    }

    private void signOut(){
        DataLocalManager.removeTokenLogin();
        startActivity(new Intent(mView.getContext(), LoginActivity.class));
        Activity activity = getActivity();
        if (activity == null) return;
        activity.finish();
    }
}
