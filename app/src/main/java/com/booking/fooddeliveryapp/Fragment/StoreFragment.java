package com.booking.fooddeliveryapp.Fragment;

import android.app.Activity;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.booking.fooddeliveryapp.R;
import com.booking.fooddeliveryapp.SharedPreferences.DataLocalManager;
import com.microsoft.signalr.HubConnection;
import com.microsoft.signalr.HubConnectionBuilder;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;


public class StoreFragment extends Fragment {
    private View view;
    String token = DataLocalManager.getTokenLogin();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.store_fragment, container, false);

        //ImageView imageView = view.findViewById(R.id.img_test);
        ListView listView = view.findViewById(R.id.lv_message);
        Button btnSend = view.findViewById(R.id.btn_send);
        EditText editText = view.findViewById(R.id.edt_message);
        Activity activity = getActivity();
        HubConnection hubConnection = HubConnectionBuilder
                .create("http://192.168.137.1:5041/chat")
                .withAccessTokenProvider(Single.defer(() -> {
                   return Single.just(token);
                }))
                .build();
        List<String> messageList = new ArrayList<String>();
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(view.getContext(),
                android.R.layout.simple_list_item_1, messageList);
        listView.setAdapter(arrayAdapter);


        hubConnection.on("ReceiveMessage", (user, message) ->
        {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    arrayAdapter.add(user + " : " + message);
                    arrayAdapter.notifyDataSetChanged();
                }
            });
        }, String.class, String.class);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String message = editText.getText().toString();
                editText.setText("");
                try {
                    hubConnection.send("SendMessage","Khang", message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        hubConnection.start();
        return view;
    }
}
