package com.booking.fooddeliveryapp.Fragment;

import static android.widget.Toast.LENGTH_SHORT;
import static android.widget.Toast.makeText;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.booking.fooddeliveryapp.CategoryAdapter;
import com.booking.fooddeliveryapp.CreateRestaurantActivity;
import com.booking.fooddeliveryapp.DetailCategory;
import com.booking.fooddeliveryapp.IOnClickListener;
import com.booking.fooddeliveryapp.LoginActivity;
import com.booking.fooddeliveryapp.R;
import com.booking.fooddeliveryapp.SharedPreferences.DataLocalManager;
import com.booking.librs.InterFaces.Methods;
import com.booking.librs.Models.BaseResponse;
import com.booking.librs.Models.Category;
import com.booking.librs.Models.CategoryCreate;
import com.booking.librs.Models.User;
import com.booking.librs.RetrofitClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {
    private TextView tvRestaurantName;
    View mView;
    private RecyclerView rcvCategory;
    Methods methods = RetrofitClient.getRetrofitInstance().create(Methods.class);
    String token = DataLocalManager.getTokenLogin();

    @Override
    public void onStart() {
        super.onStart();
        getUser();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.home_fragment, container, false);
        tvRestaurantName = mView.findViewById(R.id.tv_res_name);
        Button btnInsertCategory = mView.findViewById(R.id.btn_insert_category);
        rcvCategory = mView.findViewById(R.id.rcv_category);
        btnInsertCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View viewDialog = inflater.inflate(R.layout.dialog_create_category, container, false);
                AlertDialog.Builder builder = new AlertDialog.Builder(mView.getContext());
                builder.setCancelable(true);
                builder.setTitle("Insert new a category!");
                builder.setView(viewDialog);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        InsertCategory(viewDialog);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
                builder.show();
            }
        });
        return mView;
    }

    private void getUser() {
        Call<User> call = methods.GetUserProfile("Bearer " + token);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User user = response.body();
                System.out.println(user);
                if (user == null) {
                    makeText(mView.getContext(), "User is null", LENGTH_SHORT).show();
                    DataLocalManager.removeTokenLogin();
                    startActivity(new Intent(mView.getContext(), LoginActivity.class));
                    return;
                }
                if (user.restaurant == null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mView.getContext());
                    builder.setCancelable(true);
                    builder.setTitle("You don't have a restaurant yet!");
                    builder.setMessage("Do you want to create a restaurant?");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            startActivity(new Intent(mView.getContext(), CreateRestaurantActivity.class));
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            DataLocalManager.removeTokenLogin();
                            startActivity(new Intent(mView.getContext(), LoginActivity.class));
                            Activity activity = getActivity();
                            if (activity != null) activity.finish();
                        }
                    });
                    builder.show();
                }
                if (user.restaurant != null) {
                    tvRestaurantName.setText(user.restaurant.name);
                    setRcvCategory(mView);
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                makeText(mView.getContext(), "feo" + t, LENGTH_SHORT).show();
                startActivity(new Intent(mView.getContext(), LoginActivity.class));
            }
        });
    }

    private void setRcvCategory(View mView) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mView.getContext(), RecyclerView.VERTICAL, false);
        rcvCategory.setLayoutManager(linearLayoutManager);
        Call<List<Category>> call = methods.GetCategoryRes("Bearer " + token);
        call.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                List<Category> categories = response.body();
                CategoryAdapter categoryAdapter = new CategoryAdapter(mView.getContext(), new IOnClickListener() {
                    @Override
                    public void onClickDeleteItem(Category category) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(mView.getContext());
                        builder.setCancelable(true);
                        builder.setTitle("Do you want to delete" + category.name + "?");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                OnDeleteCategory(category);
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        });
                        builder.show();
                    }

                    @Override
                    public void onClickGoToDetail(Category category) {
                        GoToDetailCategory(category);
                    }
                });
                categoryAdapter.setData(categories);
                rcvCategory.setAdapter(categoryAdapter);
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                makeText(mView.getContext(), "feo" + t, LENGTH_SHORT).show();
            }
        });
    }

    private void InsertCategory(View view) {
        EditText edtCategoryName = view.findViewById(R.id.edt_category_name);
        String name = edtCategoryName.getText().toString();
        if (name.equals("")) {
            makeText(view.getContext(), "Name is empty!", LENGTH_SHORT).show();
            return;
        }
        CategoryCreate categoryCreate = new CategoryCreate(name);
        Call<BaseResponse> call = methods.CreateCategory("Bearer " + token, categoryCreate);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseResponse baseResponse = response.body();
                if (baseResponse == null) {
                    makeText(view.getContext(), "Response is null", LENGTH_SHORT).show();
                    return;
                }
                if (baseResponse.statusCode == 200) {
                    makeText(view.getContext(), "Success!", LENGTH_SHORT).show();
                    onStart();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                makeText(view.getContext(), "Failed", LENGTH_SHORT).show();
            }
        });

    }

    private void GoToDetailCategory(Category category) {
        Intent i = new Intent(mView.getContext(), DetailCategory.class);
        i.putExtra("cateId", category.getId().toString());
        mView.getContext().startActivity(i);
    }

    private void OnDeleteCategory(Category category) {
        Call<BaseResponse> call = methods.DeleteCategory("Bearer " + token, category.getId());
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                onStart();
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                makeText(mView.getContext(), "Delete Failed!", LENGTH_SHORT).show();
            }
        });
    }
}
