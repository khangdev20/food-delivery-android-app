package com.booking.fooddeliveryapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.booking.librs.Models.Category;
import com.booking.librs.Models.Food;
import com.booking.librs.Models.TestModel;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class FoodAdapter extends RecyclerView.Adapter<FoodAdapter.FoodViewHolder> {

    private Context mContext;
    private List<Food> foods;
    private FoodOnClick foodOnClick;

    public FoodAdapter(Context mContext, FoodOnClick foodOnClick) {
        this.mContext = mContext;
        this.foodOnClick = foodOnClick;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<Food> list) {
        this.foods = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public FoodViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_food, parent, false);
        return new FoodViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FoodViewHolder holder, int position) {
        Food food = foods.get(position);
        if (food == null) {
            return;
        }
        String COUNTRY = "VN";
        String LANGUAGE = "vi";
        String str = NumberFormat.getCurrencyInstance(new Locale(LANGUAGE, COUNTRY)).format(food.price);
        holder.tvFoodName.setText(food.getName());
        holder.tvFoodDescribe.setText(food.getDescribe());
        holder.tvFoodPrice.setText(str);
        holder.cvFoodItem.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                foodOnClick.onLongClickFood(food);
                return false;
            }
        });

        holder.cvFoodItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                foodOnClick.onClickFood(food);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (foods != null) {
            return foods.size();
        }
        return 0;
    }

    public static class FoodViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvFoodName;
        private TextView tvFoodDescribe;
        private TextView tvFoodPrice;
        private CardView cvFoodItem;

        public FoodViewHolder(@NonNull View itemView) {
            super(itemView);
            cvFoodItem = itemView.findViewById(R.id.layout_item_food);
            tvFoodName = itemView.findViewById(R.id.tv_food_name);
            tvFoodDescribe = itemView.findViewById(R.id.tv_food_describe);
            tvFoodPrice = itemView.findViewById(R.id.tv_food_price);
        }
    }
}
