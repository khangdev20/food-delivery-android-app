package com.booking.fooddeliveryapp;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.booking.fooddeliveryapp.SharedPreferences.DataLocalManager;
import com.booking.librs.InterFaces.Methods;
import com.booking.librs.Models.BaseResponse;
import com.booking.librs.Models.FoodCreate;
import com.booking.librs.RetrofitClient;

import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateFoodActivity extends AppCompatActivity {
    private EditText edtFoodName, edtFoodDescribe, edtFoodPrice;
    Methods methods = RetrofitClient.getRetrofitInstance().create(Methods.class);
    String token = DataLocalManager.getTokenLogin();

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_food);

        TextView tvCategoryName = findViewById(R.id.tv_name_category);
        Button btnSubmit = findViewById(R.id.btn_submit);
        Button btnCancel = findViewById(R.id.btn_cancel);
        edtFoodName = findViewById(R.id.edt_food_name);
        edtFoodDescribe = findViewById(R.id.edt_food_describe);
        edtFoodPrice = findViewById(R.id.edt_food_price);
        Bundle bundle = getIntent().getExtras();
        String cateId = bundle.getString("cateId");
        String cateName = bundle.getString("cateName");
        tvCategoryName.setText(cateName);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(CreateFoodActivity.this, "Clicked", Toast.LENGTH_SHORT).show();
//                InsertFood(cateId);
                System.out.println(edtFoodName.getText().toString());
                System.out.println(edtFoodDescribe.getText().toString());
                System.out.println(edtFoodPrice.getText().toString());
                FoodCreate foodCreate = new FoodCreate(
                        edtFoodName.getText().toString(),
                        edtFoodDescribe.getText().toString(),
                        Double.parseDouble(edtFoodPrice.getText().toString()),
                        UUID.fromString(cateId)
                );
                System.out.println(foodCreate);
                InsertFood(foodCreate);
            }
        });
    }

    private void InsertFood(FoodCreate foodCreate) {

        Call<BaseResponse> call = methods.CreateFood("Bearer " + token, foodCreate);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseResponse baseResponse = response.body();
                if (baseResponse == null) return;
                if (baseResponse.statusCode == 200) {
                    Toast.makeText(CreateFoodActivity.this, "Success", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                System.out.println(t);

            }
        });


    }
}