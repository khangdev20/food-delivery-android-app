package com.booking.librs.Models;

import java.util.UUID;

public class FoodCreate {
    public String name;
    public String describe;
    public double price;
    public UUID categoryId;

    public FoodCreate(String name, String describe, double price, UUID categoryId) {
        this.name = name;
        this.describe = describe;
        this.price = price;
        this.categoryId = categoryId;
    }
}
