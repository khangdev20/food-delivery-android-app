package com.booking.librs.Models;

public class TestModel {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TestModel(String name) {
        this.name = name;
    }
}
