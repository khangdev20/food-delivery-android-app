package com.booking.librs.Models;

public class RestaurantCreate {
    public String name;
    public String address;

    public RestaurantCreate(String name, String address) {
        this.name = name;
        this.address = address;
    }
}
