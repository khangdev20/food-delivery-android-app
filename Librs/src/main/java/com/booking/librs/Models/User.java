package com.booking.librs.Models;

import java.util.UUID;

public class User {
    public UUID id;
    public String name;
    public String email;
    public String phone;
    public String roles;
    public Restaurant restaurant;
}
