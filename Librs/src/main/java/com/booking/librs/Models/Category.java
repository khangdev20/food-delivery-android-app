package com.booking.librs.Models;

import java.util.ArrayList;
import java.util.UUID;

public class Category {
    public String name;
    public ArrayList<Food> foods;
    public UUID restaurantId;
    public Restaurant restaurant;
    public UUID id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Food> getFoods() {
        return foods;
    }

    public void setFoods(ArrayList<Food> foods) {
        this.foods = foods;
    }

    public UUID getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(UUID restaurantId) {
        this.restaurantId = restaurantId;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Category(String name, ArrayList<Food> foods, UUID restaurantId, Restaurant restaurant, UUID id) {
        this.name = name;
        this.foods = foods;
        this.restaurantId = restaurantId;
        this.restaurant = restaurant;
        this.id = id;
    }
}
