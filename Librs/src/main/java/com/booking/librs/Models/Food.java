package com.booking.librs.Models;

import java.util.UUID;

public class Food {
    public UUID id;
    public String name;
    public String describe;
    public double price;
    public UUID categoryId;
    public Category category;

    public Food(UUID id, String name, String describe, double price, UUID categoryId, Category category) {
        this.id = id;
        this.name = name;
        this.describe = describe;
        this.price = price;
        this.categoryId = categoryId;
        this.category = category;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public UUID getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(UUID categoryId) {
        this.categoryId = categoryId;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
