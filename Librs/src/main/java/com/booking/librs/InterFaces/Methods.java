package com.booking.librs.InterFaces;

import com.booking.librs.Models.AccessToken;
import com.booking.librs.Models.BaseResponse;
import com.booking.librs.Models.Category;
import com.booking.librs.Models.CategoryCreate;
import com.booking.librs.Models.FoodCreate;
import com.booking.librs.Models.RestaurantCreate;
import com.booking.librs.Models.User;
import com.booking.librs.Models.UserLogin;

import java.util.List;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface Methods {
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST("api/User/login")
    Call<AccessToken> Login(@Body UserLogin userLogin);

    @GET("api/User/profile")
    Call<User> GetUserProfile(@Header("Authorization") String token);

    @POST("api/Restaurant")
    Call<BaseResponse> CreateNewRes(@Header("Authorization") String token, @Body RestaurantCreate restaurant);

    @POST("api/Category")
    Call<BaseResponse> CreateCategory(@Header("Authorization") String token, @Body CategoryCreate categoryCreate);

    @GET("api/Category/category-restaurant")
    Call<List<Category>> GetCategoryRes(@Header("Authorization") String token);

    @GET("api/Category/{id}")
    Call<Category> GetCategory(@Header("Authorization") String token, @Path("id") UUID id);

    @DELETE("api/Category/{id}")
    Call<BaseResponse> DeleteCategory(@Header("Authorization") String token, @Path("id") UUID id);

    @POST("api/Food")
    Call<BaseResponse> CreateFood(@Header("Authorization") String token, @Body FoodCreate foodCreate);

    @DELETE("api/Food/{id}")
    Call<BaseResponse> DeleteFood(@Header("Authorization") String token, @Path("id") UUID id);

}
